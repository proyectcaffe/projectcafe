drop database if exists restaurante;
create database restaurante;

use restaurante;
drop table if exists mesa;
DROP TABLE IF eXISTS CAMARERO;

DROP TABLE IF EXISTS administrador;
DROP TABLE IF EXISTS empleado;
drop table if exists linped;
drop table if exists pedido;
drop table if exists plato;
drop table if exists bebida;
drop table if exists comestible;




CREATE TABLE empleado (
   nombre varchar(32),
   clave varchar(32),
   CONSTRAINT pk_usuarios PRIMARY KEY (nombre)  
) engine = innodb;

CREATE TABLE administrador
(
	nombre_admin varchar(32), 
   CONSTRAINT pk_administrador PRIMARY KEY (nombre_admin),
   Constraint fk_administrador_empleado FOREIGN KEY (nombre_admin) REFERENCES empleado(nombre)
);

/*cansado mirar si la clave ajena esta bien colocada*/
CREATE TABLE camarero
(
	nombre_camarero varchar(32),
   CONSTRAINT pk_camarero PRIMARY KEY (nombre_camarero),
   Constraint fk_camarero_empleado FOREIGN KEY (nombre_camarero) REFERENCES empleado(nombre)
  
);
CREATE TABLE mesa
(
	id_mesa varchar(32),
	url_imagen varchar(32),
	camarero_encargado varchar(32) not null,
	disponible bool,
	CONSTRAINT pk_mesa PRIMARY KEY (id_mesa),
	Constraint fk_mesa_camarero FOREIGN KEY (camarero_encargado) REFERENCES camarero(nombre_camarero)
);

CREATE TABLE PEDIDO
(
	id_pedido varchar(32),
	id_mesa varchar(32),
	CONSTRAINT pk_pedido PRIMARY KEY (id_pedido),
	CONSTRAINT fk_pedido_mesa foreign key (id_mesa) references mesa(id_mesa)
);


CREATE TABLE LINPED
(
	id_linea varchar(32) ,
	precio double,
	cantidad int,
	id_comestible varchar(32),
	id_pedido varchar(32) not null,
	constraint pk_linped primary key(id_linea,id_pedido),
	Constraint fk_linped_pedido FOREIGN KEY (id_pedido) REFERENCES pedido(id_pedido)
);


Create table comestible
(
	id_comestible varchar(32),
	pvp double,
	url_imagen varchar(32),
	nombre varchar(32),
	constraint pk_comestible primary key(id_comestible)
);

CREATE TABLE bebida
(
	id_bebida varchar(32),
	gaseosa bool, 
   CONSTRAINT pk_bebida PRIMARY KEY (id_bebida),
   Constraint fk_bebida_comestible FOREIGN KEY (id_bebida) REFERENCES comestible(id_comestible)
);

CREATE TABLE plato
(
	id_plato varchar(32),
	carne bool, 
   CONSTRAINT pk_plato PRIMARY KEY (id_plato),
   Constraint fk_plato_comestible FOREIGN KEY (id_plato) REFERENCES comestible(id_comestible)
);

 CREATE TABLE postre
(
	id_postre varchar(32),
	carne bool, 
   CONSTRAINT pk_postre PRIMARY KEY (id_postre),
   Constraint fk_postre_comestible FOREIGN KEY (id_postre) REFERENCES comestible(id_comestible)
);

CREATE TABLE aperitivo
(
	id_aperitivo varchar(32),
	carne bool, 
   CONSTRAINT pk_aperitivo PRIMARY KEY (id_aperitivo),
   Constraint fk_aperitivo_comestible FOREIGN KEY (id_aperitivo) REFERENCES comestible(id_comestible)
);

/*Para insertar datos ... 
En admin, camarero tiene que existir antes un usuario */

INSERT INTO empleado (nombre, clave) VALUES ('admin', '1234');
INSERT INTO administrador (nombre_admin) VALUES ('admin');
INSERT INTO empleado (nombre, clave) VALUES ('manolo', 'pass');
INSERT INTO camarero (nombre_camarero) VALUES ('manolo');
INSERT INTO mesa (id_mesa, camarero_encargado, disponible) VALUES ('001', 'manolo', true);
INSERT INTO mesa (id_mesa, camarero_encargado, disponible) VALUES ('002', 'manolo', true);
INSERT INTO mesa (id_mesa, camarero_encargado, disponible) VALUES ('003', 'manolo', true);
INSERT INTO mesa (id_mesa, camarero_encargado, disponible) VALUES ('004', 'manolo', true);
INSERT INTO mesa (id_mesa, camarero_encargado, disponible) VALUES ('005', 'manolo', true);
INSERT INTO mesa (id_mesa, camarero_encargado, disponible) VALUES ('006', 'manolo', true);
INSERT INTO mesa (id_mesa, camarero_encargado, disponible) VALUES ('007', 'manolo', true);
INSERT INTO mesa (id_mesa, camarero_encargado, disponible) VALUES ('008', 'manolo', true);
INSERT INTO pedido (id_pedido,id_mesa) VALUES ("001","001");
 
 
/*Para insertar datos ... 
En admin, camarero tiene que existir antes un usuario */

/*metiendo hamburguesas*/
INSERT INTO comestible
VALUES ("001",5.00 ,"burger1.png","Burger N1");

INSERT INTO comestible
VALUES ("002",4.00 ,"burger2.png","Burger N2");

INSERT INTO comestible
VALUES ("003",4.40 ,"burger3.png","Burger N3");

INSERT INTO comestible
VALUES ("004",5.30 ,"burger4.png","Burger N4");

INSERT INTO comestible
VALUES ("005",4.20 ,"burger5.png","Burger N5");

INSERT INTO comestible
VALUES ("006",5.20 ,"burger6.png","Burger N6");

INSERT INTO comestible
VALUES ("007",4.20 ,"burger7.png","Burger N7");

INSERT INTO comestible
VALUES ("008",2.20 ,"burger8.png","Burger N8");

INSERT INTO comestible
VALUES ("009",4.20 ,"burger9.png","Burger N9");


insert into plato
values ("001",true);
insert into plato
values("002",true);
insert into plato
values ("003",true);
insert into plato
values("004",true);
insert into plato
values ("005",true);
insert into plato
values("006",true);
insert into plato
values ("007",true);
insert into plato
values("008",true);
insert into plato
values ("009",true);



/*metiendo bebidas*/
INSERT INTO comestible
VALUES ("010",1.40 ,"bebidas1.png","Bebida N1");


INSERT INTO comestible
VALUES ("011",1.20 ,"bebidas2.png","Bebida N2");

INSERT INTO comestible
VALUES ("012",1.30 ,"bebidas3.png","Bebida N3");

INSERT INTO comestible
VALUES ("013",1.70 ,"bebidas4.png","Bebida N4");

INSERT INTO comestible
VALUES ("014",1.10 ,"bebidas5.png","Bebida N5");

INSERT INTO comestible
VALUES ("015",2.10 ,"bebidas6.png","Bebida N6");

INSERT INTO comestible
VALUES ("016",2.40 ,"bebidas7.png","Bebida N7");

INSERT INTO comestible
VALUES ("017",3.40 ,"bebidas8.png","Bebida N8");

INSERT INTO comestible
VALUES ("018",0.80 ,"bebidas9.png","Bebida N9");



insert into bebida
values("010",false);


insert into bebida
values("011",false);

insert into bebida
values("012",false);

insert into bebida
values("013",false);

insert into bebida
values("014",false);

insert into bebida
values("015",false);

insert into bebida
values("016",false);

insert into bebida
values("017",false);

insert into bebida
values("018",false);




/*Insertando postres*/

INSERT INTO comestible
VALUES ("019",0.80 ,"postre1.png","Postre N1");

INSERT INTO comestible
VALUES ("020",0.90 ,"postre2.png","Postre N2");

INSERT INTO comestible
VALUES ("021",1.90 ,"postre3.png","Postre N3");

INSERT INTO comestible
VALUES ("022",0.95 ,"postre4.png","Postre N4");

INSERT INTO comestible
VALUES ("023",1.50 ,"postre5.png","Postre N5");

INSERT INTO comestible
VALUES ("024",1.20 ,"postre6.png","Postre N6");

INSERT INTO comestible
VALUES ("025",2.00 ,"postre7.png","Postre N7");

INSERT INTO comestible
VALUES ("026",4.00 ,"postre8.png","Postre N8");

INSERT INTO comestible
VALUES ("027",2.00 ,"postre9.png","Postre N9");


insert into postre
values("019",false);

insert into postre
values ("020",false);

insert into postre
values ("021",false);

insert into postre
values("022",false);

insert into postre
values ("023",false);

insert into postre
values("024",false);

insert into postre
values("025",false);

insert into postre
values("026",false);

insert into postre
values("027",false);


/*Insertanodo aperitivos*/


INSERT INTO comestible
VALUES ("028",1.80 ,"aperitivo1.png","Aperitivo N1");

INSERT INTO comestible
VALUES ("029",1.90 ,"aperitivo2.png","Aperitivo N2");

INSERT INTO comestible
VALUES ("030",1.80 ,"aperitivo3.png","Aperitivo N3");

INSERT INTO comestible
VALUES ("031",0.95 ,"aperitivo4.png","Aperitivo N4");

INSERT INTO comestible
VALUES ("032",1.50 ,"aperitivo5.png","Aperitivo N5");

INSERT INTO comestible
VALUES ("033",1.20 ,"aperitivo6.png","Aperitivo N6");

INSERT INTO comestible
VALUES ("034",2.00 ,"aperitivo7.png","Aperitivo N7");

INSERT INTO comestible
VALUES ("035",4.00 ,"aperitivo8.png","Aperitivo N8");

INSERT INTO comestible
VALUES ("036",2.00 ,"aperitivo9.png","Aperitivo N9");


insert into aperitivo
values("028",false);

insert into aperitivo
values ("029",false);

insert into aperitivo
values ("030",false);

insert into aperitivo
values("031",false);

insert into aperitivo
values ("032",false);

insert into aperitivo
values("033",false);

insert into aperitivo
values("034",false);

insert into aperitivo
values("035",false);

insert into aperitivo
values("036",false);
