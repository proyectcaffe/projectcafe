package tpvtest;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class TablaPedidoModel extends AbstractTableModel {

    ArrayList<LineaDePedido> data;
    String[] header;

    public TablaPedidoModel(ArrayList<LineaDePedido> data, String[] header) {
        this.header = header;
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex)
        {
            case 0:
                return data.get(rowIndex).getProducto().getNombre();
            case 1:
                return data.get(rowIndex).getCantidad();
            case 2:
                return String.format("%.2f", data.get(rowIndex).getProducto().getPrecio());
            case 3:
                return String.format("%.2f", data.get(rowIndex).Total());
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int index) {
        return header[index];
    }
}