package tpvtest;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
/**
 * @author lucas y toni
 */
public class Catalogo implements ActionListener{
    ArrayList<Producto> productos; 
    int numeroPlatos;
    int numeroBebidas;
    int numeroPostres;
    int numeroAperitivos;
    // Este pedido será inicializado con cada mesa y estará asociado a ella.
    Pedido pedido;
    
    //Elementos Swing
    JPanel panelCatalogo;
    static JPanel panelPlatos, panelBebidas,panelPostres,panelAperitivos;
    JTabbedPane tabbedPane;
    
    public Catalogo() throws SQLException
    {
        productos = new ArrayList(); 
        
        //Inicializamos elementos Swing
        panelCatalogo = new JPanel();
        tabbedPane = new JTabbedPane();
        panelPlatos = new JPanel();
        panelPlatos.setLayout(new GridLayout(numeroPlatos, 3, 10, 10));
        panelPlatos.setName("PLATOS");
        
        panelBebidas = new JPanel();
        panelBebidas.setLayout(new GridLayout(numeroBebidas, 3, 10, 10));
        panelBebidas.setName("BEBIDAS");
        
        panelPostres = new JPanel();
        panelPostres.setLayout(new GridLayout(numeroPostres, 3, 10, 10));
        panelPostres.setName("POSTRES");
        
        panelAperitivos = new JPanel();
        panelAperitivos.setLayout(new GridLayout(numeroPostres, 3, 10, 10));
        panelAperitivos.setName("APERITIVO");
        
        tabbedPane.add(panelPlatos);
        tabbedPane.add(panelBebidas);
        tabbedPane.add(panelPostres);
        tabbedPane.add(panelAperitivos);
        
        // Función cargar productos de la base de datos
        initBebidas();
        initPlatos();
        initPostres();
        initAperitivos();
        // Iniciando botones de bebidas
         for(int i=0; i<this.productos.size();i++)
        {
            this.productos.get(i).boton.addActionListener((ActionListener) this);
            productos.get(i).addToPanel();
        }
    }
    
    public void add(Producto producto) {
        productos.add(producto);
    }
    
    public void setNumeroBebidas() {
        DatabaseConnection conexionBD = new DatabaseConnection();
        
        // Busca en la base de datos la cantidad de registros en la tabla mesas
        numeroBebidas = conexionBD.CompruebaNumeroDeRegistros("bebida");
        System.out.println("Numero bebidas: " + numeroBebidas);
    }
        
    public void initBebidas() throws SQLException {
        setNumeroBebidas();
        // Creamos array de mesas
       DatabaseConnection conexion = new DatabaseConnection();
        String[] ids =conexion.getDatosBebidas(numeroBebidas);
        
        for (int bebida = 0; bebida < numeroBebidas; bebida++)
        {
            String[] datosBebidaActual = ids[bebida].split("@");
            
            double pvpBebidaActual = Double.parseDouble(datosBebidaActual[1]);
            Producto bebidaActual = new Bebida(
                    datosBebidaActual[0],
                    pvpBebidaActual,
                    datosBebidaActual[2],
                    datosBebidaActual[3]
            );
            System.out.println(bebidaActual.toString());
            
            this.add(bebidaActual);
        }
    }
    
    public void initPlatos() 
    {
        setNumeroPlatos();
        
       DatabaseConnection conexion = new DatabaseConnection();
        String[] ids =conexion.getDatosPlatos(numeroPlatos);
        
        for (int plato = 0; plato < numeroPlatos; plato++)
        {
            String[] datosPlatoActual = ids[plato].split("@");
            
            double pvpPlatoActual = Double.parseDouble(datosPlatoActual[1]);
            Producto platoActual = new Plato(
                    datosPlatoActual[0],
                    pvpPlatoActual,
                    datosPlatoActual[2],
                    datosPlatoActual[3]
            );
            System.out.println(platoActual.toString());
            
            this.add(platoActual);
        }
    }
    
    public void initPostres() 
    {
        setNumerosPostres();
        DatabaseConnection conexion = new DatabaseConnection();
        String[] ids =conexion.getDatosPostre(numeroPostres);
        
        for (int postre = 0; postre < numeroPostres; postre++)
        {
            String[] datosPlatoActual = ids[postre].split("@");
            
            double pvpPlatoActual = Double.parseDouble(datosPlatoActual[1]);
            Producto platoActual = new Postre(
                    datosPlatoActual[0],
                    pvpPlatoActual,
                    datosPlatoActual[2],
                    datosPlatoActual[3]
            );
            System.out.println(platoActual.toString());
            
            this.add(platoActual);
        }
    }
    
    public void initAperitivos() 
    {
        setNumerosApertivos();
        DatabaseConnection conexion = new DatabaseConnection();
        String[] ids =conexion.getDatosAperitivo(numeroAperitivos);
        
        for (int postre = 0; postre < numeroAperitivos; postre++)
        {
            String[] datosPlatoActual = ids[postre].split("@");
            
            double pvpPlatoActual = Double.parseDouble(datosPlatoActual[1]);
            Producto platoActual = new Aperitivo(
                    datosPlatoActual[0],
                    pvpPlatoActual,
                    datosPlatoActual[2],
                    datosPlatoActual[3]
            );
            System.out.println(platoActual.toString());
            
            this.add(platoActual);
        }
    }
    
    
    
    
    public void setNumeroPlatos() {
        DatabaseConnection conexionBD = new DatabaseConnection();
        
        // Busca en la base de datos la cantidad de registros en la tabla mesas
        numeroPlatos = conexionBD.CompruebaNumeroDeRegistros("plato");
        System.out.println("Numero platos: "+ numeroPlatos);
    }
    
    public void setNumerosPostres()
    {
        DatabaseConnection conexionBD = new DatabaseConnection();
        numeroPostres = conexionBD.CompruebaNumeroDeRegistros("postre");
        System.out.println("Numero postres: "+ numeroPostres);
    }
    
    
    public void setNumerosApertivos()
    {
        DatabaseConnection conexionBD = new DatabaseConnection();
        numeroAperitivos = conexionBD.CompruebaNumeroDeRegistros("aperitivo");
        System.out.println("Numero postres: "+ numeroAperitivos);
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
      
    }
}
