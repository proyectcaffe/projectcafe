/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpvtest;

import java.util.ArrayList;

/**
 * @author toni y lucas
 */
public class Pedido {
    String id_pedido;
    ArrayList<LineaDePedido> lineasDePedido;
    
    public Pedido() {
        lineasDePedido = new ArrayList<>();
    }
    
    public double Total() {
        double total = 0;
        for (int linped = 0; linped < lineasDePedido.size(); linped++)
        {
            total += lineasDePedido.get(linped).Total();
        }
        return total;
    }
    
    public void addLineaDePedido(LineaDePedido linped) {
        lineasDePedido.add(linped);
    }
    
    public void cobrar() {
        
    }
}
