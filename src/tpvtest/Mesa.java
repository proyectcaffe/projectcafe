package tpvtest;

import javax.swing.JOptionPane;
import java.awt.FlowLayout;
import java.sql.SQLException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author lucas
 */
public class Mesa extends Catalogo{
    JFrame frameMesa;
    JPanel panelPrincipal;
    JPanel panelPedido;
    JButton boton;
    JButton botonCobrar;
    
    String id_mesa;
    int disponible;
    
    public Mesa(String id) throws SQLException
    {
        this.id_mesa = id;
        initBoton("img");
        disponible=0;
        this.pedido = new Pedido();
        initMesa();
    }
    
    public void initMesa() throws SQLException
    {
        frameMesa = new JFrame();
        frameMesa.setTitle("MESA " + id_mesa);
        frameMesa.setSize(1000, 600);
        frameMesa.setVisible(false);
        frameMesa.setLayout(new FlowLayout());
        
        panelPrincipal = new JPanel();
        panelPedido = new JPanel();
        panelPedido.setLayout(new BoxLayout(panelPedido, BoxLayout.Y_AXIS));
        panelPrincipal.add(panelCatalogo);
        panelPrincipal.add(panelPedido);
        panelCatalogo.add(tabbedPane);
        
        frameMesa.add(panelPrincipal);
        initTablaPedido();
    }
    
    public void initBoton(String carpeta) {
        this.boton = new JButton("Mesa " + this.id_mesa);
        // Añadimos imagen
        try {
            Image img = ImageIO.read(
                    getClass().getResource(carpeta + "/mesas.png"));
            this.boton.setIcon(new ImageIcon(img));
        } 
        catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    public void showMesa()
    {
        frameMesa.setVisible(true);
    }
    
    public void HideMesa()
    {
        frameMesa.setVisible(false);

    }
    
    public void initTablaPedido()
    {
        // Tabla pedido
        TablaPedidoModel modeloTablaPedido 
                = new TablaPedidoModel(
                        pedido.lineasDePedido, 
                        new String[]{
                            "Nombre", 
                            "Cantidad", 
                            "Precio Unitario", 
                            "Total Linea"});

        JTable tabla = new JTable(modeloTablaPedido);

        // Metemos la tabla en el scrollpane para que se vea las cabeceras
        JScrollPane scrollpane = new JScrollPane(tabla);
        panelPedido.add(scrollpane);
        panelPedido.add(new JLabel("TOTAL: " + String.format( "%.2f", pedido.Total())+"€"));
        botonCobrar = new JButton("COBRAR");
        botonCobrar.addActionListener((ActionListener)this);
        botonCobrar.setVisible(!(pedido.lineasDePedido.size() == 0));
        panelPedido.add(botonCobrar);
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        // Al clicar sobre el botón de cobrar
        if (event.getSource() == this.botonCobrar) {
            System.out.println("Has clicado sobre el boton cobrar");
            
            int opcion = JOptionPane.showConfirmDialog(null, "Confirmar cobro? " 
                    + this.pedido.Total()+"€", "Cobrar", JOptionPane.YES_NO_OPTION);

            if (opcion == 0) { //The ISSUE is here
               System.out.print("Sí");
               this.pedido = new Pedido();
                refreshPedido();
            } else {
               System.out.print("No");
            }
        }
        
        // Al clicar sobre un producto
        for (int i = 0; i < this.productos.size(); i++)
        {
            if (event.getSource() == this.productos.get(i).boton)
            {
                boolean encontrado = false;
                for (LineaDePedido lineaDePedido : pedido.lineasDePedido) {
                    if (lineaDePedido.producto == this.productos.get(i))
                    {
                        lineaDePedido.addUnidad();
                        encontrado = true;
                    }
                }
                
                if (!encontrado)
                {
                    LineaDePedido linped = new LineaDePedido(this.productos.get(i));
                    pedido.addLineaDePedido(linped);
                }
                
                refreshPedido();
            }
        }
    }
    
    public void refreshPedido() {
        // Actualizamos datos
        panelPedido.removeAll();
        initTablaPedido();

        //Recarga el panel de Pedidos
        panelPedido.revalidate();
        panelPedido.repaint();
    }

}
