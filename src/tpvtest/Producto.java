package tpvtest;

import java.awt.Image;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author lucas
 */
public class Producto {
    String id_producto;
    double precio;
    String imagen;
    String nombre;
    
    JButton boton;
    JPanel panel;
    
    public String getNombre(){
        return this.nombre;
    }
    
    public double getPrecio() {
        return this.precio;
    }
    
    public Producto(String id_producto, double precio, String imagen, String nombre)
    {
        this.id_producto = id_producto;
        this.nombre = nombre;
        this.precio = precio;
        this.imagen = imagen;
        initBoton("img");
    }
    
    @Override
    public String toString(){
        return "Producto: " + nombre;
    }
    
    public void addToPanel(){
        panel.add(this.boton);
    }
    
    public void initBoton(String carpeta) {
        this.boton = new JButton(nombre);
        // Añadimos imagen
        try {
            Image img = ImageIO.read(
                    getClass().getResource(carpeta + "/" + this.imagen));
            this.boton.setIcon(new ImageIcon(img));
        } 
        catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
