/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpvtest;

/**
 * @author Usuario
 */
public class Empleado {
    private String nombre;
    private String clave;
    
    public Empleado(String nombre,String clave)
    {
        this.nombre=nombre;
        this.clave=clave;
    }
    
    public void SetClave(String clave)
    {
        this.clave=clave;
    }
    
    public String GetClave()
    {
        return clave;
    }
    
    public void SetNombre(String nombre)
    {
        this.nombre=nombre;
    }
    
    public String GetNombre()
    {
        return nombre;
    }
}
