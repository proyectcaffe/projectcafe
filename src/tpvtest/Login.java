package tpvtest;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author lucas amd toni
 */
public class Login implements ActionListener {
    JFrame loginFrame;
    JLabel userLabel, passLabel;
    JTextField user, pass;
    JButton loginButton;
    JPanel panelPrincipal, panelImagen, panelDatos;
    
    public Login() {
        initComponents();
    }
    
    public void initComponents() {
        loginFrame = new JFrame("Login");
        loginFrame.setSize(500, 300);
        loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        panelPrincipal = new JPanel();
        panelPrincipal.setSize(500, 300);
        loginFrame.add(panelPrincipal);
        
        userLabel = new JLabel("Usuario");
        passLabel = new JLabel ("Contraseña");
        user = new JTextField();
        pass = new JPasswordField();
        loginButton = new JButton("Entrar");
        loginButton.addActionListener(this);
        
        userLabel.setBounds(10, 10, 120, 20);
        passLabel.setBounds(10, 10, 120, 20);
        user.setBounds(140, 10, 200, 20);
        pass.setBounds(140, 30, 200, 20);
        loginButton.setBounds(140, 55, 100, 20);
        
        panelImagen = new JPanel();
                
        panelDatos = new JPanel();
        panelDatos.setSize(250, 150);
        panelDatos.setLayout(new GridLayout(5, 1, 10, 10));
        panelDatos.add(userLabel);
        panelDatos.add(user);
        panelDatos.add(passLabel);
        panelDatos.add(pass);
        panelDatos.add(loginButton);
        
        panelPrincipal.add(panelImagen);
        panelPrincipal.add(panelDatos);
        
        
        loginFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == loginButton) {
            //por ahora siempre que vayamos a crear un usuario este
            // de tipo camarero
            if (compruebaDatos() == 0){
                Camarero cam= new Camarero(user.getText(), pass.getText());
                try {
                    Restaurante restaurante = new Restaurante(cam);
                } catch (SQLException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
                //restaurante.SetResponsable(cam);
                loginFrame.dispose();
            }
        }
    }
    
    public int compruebaDatos(){
        DatabaseConnection conexionBD = new DatabaseConnection();
        return conexionBD.CompruebaLogin(user.getText(), pass.getText());
    }
}
