package tpvtest;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author lucas
 */
public  class Restaurante implements ActionListener{
    JFrame restauranteFrame;
    JPanel panelTodo;
    JPanel panelUser, panelMesas;
    
    Mesa[] mesas;
    int numeroDeMesas;
    Camarero responsable;
    Catalogo catalogo;
    
    public Restaurante(Camarero cam) throws SQLException {
        this.responsable=cam;
        initComponents();
    }

    public void initComponents() throws SQLException {
        restauranteFrame = new JFrame();
        restauranteFrame.setTitle("Restaurante - Project Café");
        restauranteFrame.setSize(800, 600);
        restauranteFrame.setVisible(true);
        restauranteFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        restauranteFrame.setLayout(new FlowLayout());
        
        panelTodo = new JPanel();
        restauranteFrame.add(panelTodo);
        
        //Inicializar número de mesas y crear los paneles
        initMesas();
        panelMesas = new JPanel();
        panelMesas.setLayout(new GridLayout(3,numeroDeMesas, 10, 10));
        
        for(int i=0; i<numeroDeMesas;i++)
        {
            mesas[i].boton.addActionListener(this);
            panelMesas.add(mesas[i].boton);
        }
        panelUser = new JPanel();
        panelUser.add(new JButton("Opciones de usuario "+ responsable.GetNombre() ));
        panelTodo.add(panelUser);
        panelTodo.add(panelMesas);
        
        //Inicializar Catálogo.
        catalogo = new Catalogo();
    }
    
    public void setNumeroDeMesas() {
        DatabaseConnection conexionBD = new DatabaseConnection();
        
        // Busca en la base de datos la cantidad de registros en la tabla mesas
        numeroDeMesas = conexionBD.CompruebaNumeroDeRegistros("mesa");
        System.out.println(numeroDeMesas);
    }
    
    public void initMesas() throws SQLException {
        setNumeroDeMesas();
        // Creamos array de mesas
        mesas = new Mesa[numeroDeMesas];
        for (int i = 0; i < mesas.length; i++)
        {
            String num = "" + i;
            mesas[i] = new Mesa(num);
        }
        
        //Consultamos a la base de datos para los datos de las mesas
        DatabaseConnection conexion = new DatabaseConnection();
        String[][] ids =conexion.getDatosMesas(numeroDeMesas);
        for (int i= 0; i < ids.length; i++)
        {
            System.out.println(ids[0][i]);
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        for (int i = 0; i< numeroDeMesas; i++)
        {
            if (event.getSource() == mesas[i].boton) {
              //por ahora siempre que vayamos a crear un usuario este
              // de tipo camarero
              mesas[i].frameMesa.setVisible(true);
            }
        }
    }
}