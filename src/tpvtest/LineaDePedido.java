package tpvtest;

/**
 * @author lucas and toni
 */
public class LineaDePedido {
    Producto producto;
    int cantidad;
    
    public LineaDePedido(Producto producto) {
        this.producto = producto;
        cantidad = 1;
    }
    
    public void addUnidad() {
        cantidad++;
    }
    
    public void eliminarProducto() {
        if (cantidad > 1)
            cantidad--;
    }
    
    public Producto getProducto() {
        return this.producto;
    }
    
    public int getCantidad() {
        return this.cantidad;
    }
    
    public double Total() {
        return (producto == null) ? 0 : producto.precio * cantidad;
    }
    
    @Override
    public String toString() {
        return (producto.nombre 
                + "   ------------------------   "
                + producto.precio + "€" + "   ----   " 
                + cantidad);
    }
}
