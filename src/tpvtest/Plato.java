/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpvtest;

/**
 *
 * @author lucas y toni
 */

public class Plato extends Producto {
    boolean carne;

    public Plato(String id_producto, double precio, String imagen, String nombre) {
        super(id_producto, precio, imagen, nombre);
        this.carne = false;
        panel = Catalogo.panelPlatos;
    }
}
