package tpvtest;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class DatabaseConnection {
    Connection connection;
    PreparedStatement consulta;
    ResultSet datos;
    String passMySql = "rootpass";
    
    public Connection getConnection(String BD,String usuario, String pass) throws SQLException {
        try {
            //Llamando a la clase o driver de jdbc
            Class.forName("com.mysql.jdbc.Driver");
            //Conexión para la base de datos
            String servidor = "jdbc:mysql://localhost/" + BD;
            
            connection = (Connection) DriverManager.getConnection(
                    servidor, usuario, pass);
        }
        catch(ClassNotFoundException e ) {
            System.err.println("No se ha encontrado Driver");
            connection = null;
        }
        catch(SQLException ex){
            System.err.println("No se ha encontrado base de datos");
            connection = null;
        }
        return connection;
    }
    
    public int CompruebaNumeroDeRegistros(String tabla){
        int num = 0;
        try {
            String countNumMesas = "select * from " + tabla + ";"; //TODO
            connection = (Connection) this.getConnection("restaurante", "root", passMySql);
            consulta = (PreparedStatement) connection.prepareStatement(countNumMesas);
            datos = (ResultSet) consulta.executeQuery();
            System.out.println(datos.toString());
            while(datos.next()){
                num++;
            }
        }
        catch (SQLException e){
            System.err.println("ERROR: No se pudo conectar o hacer la consulta");
        }
        finally {
            desconectar();
            return num;
        }
    }
    
    public String[] getDatosPlatos(int numPlatos)
    {
        String[] ids = null;
        try 
        {
            ids = new String[numPlatos];
            int pos = 0;
            String select = "select p.id_plato, c.pvp,c.url_imagen,c.nombre " +
                             "from plato p,comestible c " +
                             "where p.id_plato=c.id_comestible;";
            
            connection = (Connection) this.getConnection("restaurante", "root", passMySql);
            consulta = (PreparedStatement) connection.prepareStatement(select);
            //ejecutamos la query y la guardamos en datos
            datos = (ResultSet) consulta.executeQuery();
            while(datos.next()){
                ids[pos] = datos.getString("p.id_plato")+"@"+datos.getString("c.pvp")+"@"
                        +datos.getString("c.url_imagen")+
                        "@"+datos.getString("c.nombre");
                System.out.println(ids[pos]);
                //TODO
                pos++;
            }
        }
        catch(SQLException e)
        {
            System.err.println("ERROR: No se pudo conectar o hacer la consulta");
        }
        finally
        {
            desconectar();
            return ids;
        }
    }
    
    public String[] getDatosPostre(int numPostres)
    {
        String[] ids = null;
        try 
        {
            ids = new String[numPostres];
            int pos = 0;
            String select = "select p.id_postre, c.pvp,c.url_imagen,c.nombre " +
                             "from postre p,comestible c " +
                             "where p.id_postre=c.id_comestible;";
            
            connection = (Connection) this.getConnection("restaurante", "root", passMySql);
            consulta = (PreparedStatement) connection.prepareStatement(select);
            //ejecutamos la query y la guardamos en datos
            datos = (ResultSet) consulta.executeQuery();
            while(datos.next()){
                ids[pos] = datos.getString("p.id_postre")+"@"+datos.getString("c.pvp")+"@"
                        +datos.getString("c.url_imagen")+
                        "@"+datos.getString("c.nombre");
                System.out.println(ids[pos]);
                //TODO
                pos++;
            }
        }
        catch(SQLException e)
        {
            System.err.println("ERROR: No se pudo conectar o hacer la consulta");
        }
        finally
        {
            desconectar();
            return ids;
        }
    }
    
    
     public String[] getDatosAperitivo(int numPostres)
    {
        String[] ids = null;
        try 
        {
            ids = new String[numPostres];
            int pos = 0;
            String select = "select p.id_aperitivo, c.pvp,c.url_imagen,c.nombre " +
                             "from aperitivo p,comestible c " +
                             "where p.id_aperitivo=c.id_comestible;";
            
            connection = (Connection) this.getConnection("restaurante", "root", passMySql);
            consulta = (PreparedStatement) connection.prepareStatement(select);
            //ejecutamos la query y la guardamos en datos
            datos = (ResultSet) consulta.executeQuery();
            while(datos.next()){
                ids[pos] = datos.getString("p.id_aperitivo")+"@"+datos.getString("c.pvp")+"@"
                        +datos.getString("c.url_imagen")+
                        "@"+datos.getString("c.nombre");
                System.out.println(ids[pos]);
                //TODO
                pos++;
            }
        }
        catch(SQLException e)
        {
            System.err.println("ERROR: No se pudo conectar o hacer la consulta");
        }
        finally
        {
            desconectar();
            return ids;
        }
    }
    
    
    
    
    public String[] getDatosBebidas(int numBebidas)
    {
        String[] ids = null;
        try 
        {
            ids = new String[numBebidas];
            int pos = 0;
            String select = "select b.id_bebida, c.pvp,c.url_imagen,c.nombre " +
                            "from bebida b,comestible c " +
                            "where b.id_bebida=c.id_comestible;";
            
            connection = (Connection) this.getConnection("restaurante", "root", passMySql);
            consulta = (PreparedStatement) connection.prepareStatement(select);
            //ejecutamos la query y la guardamos en datos
            datos = (ResultSet) consulta.executeQuery();
            while(datos.next()){
                ids[pos] = datos.getString("b.id_bebida")+"@"+datos.getString("c.pvp")+"@"
                        +datos.getString("c.url_imagen")+
                        "@"+datos.getString("c.nombre");
                System.out.println(ids[pos]);
                //TODO
                pos++;
            }
        }
        catch(SQLException e)
        {
            System.err.println("ERROR: No se pudo conectar o hacer la consulta");
        }
        finally
        {
            desconectar();
            return ids;
        }
    }
    
    public String[][] getDatosMesas(int numMesas )
    {
        String[][] ids = null;
        try 
        {
            ids = new String[2][numMesas];
            int pos = 0;
            String select = "select id_mesa from mesa ";
            connection = (Connection) this.getConnection("restaurante", "root", passMySql);
            consulta = (PreparedStatement) connection.prepareStatement(select);
            //ejecutamos la query y la guardamos en datos
            datos = (ResultSet) consulta.executeQuery();
            while(datos.next()){
                ids[0][pos] = datos.getString("id_mesa") ;
                pos++;
            }
        }
        catch(SQLException e)
        {
            System.err.println("ERROR: No se pudo conectar o hacer la consulta");
        }
        finally
        {
            desconectar();
            return ids;
        }
    }
    
    public int CompruebaLogin(String nombre,String clave){
        int code = 0;
        try {
            String select = "select * from empleado "
                    + "where nombre = '" + nombre
                    + "' and clave = '" + clave + "';";
            connection = (Connection) this.getConnection("restaurante", "root", passMySql);
            consulta = (PreparedStatement) connection.prepareStatement(select);
            //ejecutamos la query y la guardamos en datos
            datos = (ResultSet) consulta.executeQuery();
            if(datos.next()){
                JOptionPane.showMessageDialog(null,"Usuario aceptado" );
                
            }
            else {
                JOptionPane.showMessageDialog(null,"Usuario incorrecto" );
                code = 1;
                /*
                Mientras haya datos en la tabla, los va imprimiendo todos en una fila
                while(datos.next()){
                System.out.println("Usuario "+datos.getString("usuario") +"\n"
                                    +"Constraseña "+datos.getString("clave"));
                
                */
            }
        }
        catch(SQLException e){
            System.err.println("ERROR: No se pudo conectar o hacer la consulta");
            code = 2;
        }
        finally{
            desconectar();
            return code;
        }
    }
    
    public void desconectar (){
        try{
            connection.close();
            consulta.close();
            datos = null;
        } 
        catch(Exception e){
            // Error al desconectar
        }
    }
}
